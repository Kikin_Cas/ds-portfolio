library(tidyverse)

ggplot(data = mpg) + 
  geom_point(mapping = aes(x = displ, y = hwy))

mpg
?mpg

ggplot(data = mpg) + geom_point(mapping = aes(x = hwy, y = cyl))

ggplot(data = mpg) + geom_point(mapping = aes(x = class, y = drv))

ggplot(data = mpg) +   geom_point(mapping = aes(x = displ, y = hwy, color = class))


library(pcalg)

?backdoor

## Matriz de covarianza para una regresión lineal
# La covarianza es que tan relacionados está una variable con la otra (linealmente)
## ¿Por qué solucionando el lm.cov te da el coeficiente beta.hat?
# porque resuelve una ecuación de tipo ax = b, que también se puede ver como beta*x=b, ya que beta es la pendiente
## conjunto de ajuste (dsepset)
## Matriz de efectos causales para cada MAG

x<-matrix(1:9,3,3)
x
a<-2
a
b<-3
b
lm.cov <- function (C, y, x) {
  solve(C[x, x], C[x, y, drop = FALSE])[1, ]
}
lm.cov2 <- function (C, y, x) {
  solve(C[x, x], C[x, y, drop = FALSE])
}

beta.hat <- lm.cov(x,a,b)
beta.hat
cat("\nbeta.hat: ", class(beta.hat),"\n")
beta.hat2 <- lm.cov2(x,a,b)
beta.hat2
cat("\nbeta.hat2: ", class(beta.hat2),"\n")

# Dar respuestas más formales, escribirlo y demostrarlo
# https://stats.stackexchange.com/questions/107597/is-there-a-way-to-use-the-covariance-matrix-to-find-coefficients-for-multiple-re
# Por qué se puede usar la covarianza
# LV IDA sólo global
# LV IDA global especial, que devuelva los MAGS y abajo la matriz con los efectos causales

#
# 1. Generate some data.
#
n <- 5        # Data set size
p <- 2         # Number of regressors
set.seed(17)
z <- matrix(rnorm(n*(p+1)), nrow=n, dimnames=list(NULL, paste0("x", 1:(p+1))))
z
y <- z[, p+1]
y
x <- z[, -(p+1), drop=FALSE]; 
x
#
# 2. Find the OLS coefficients from the covariances only.
#
a <- cov(x)
a
b <- cov(x,y)
b
beta.hat <- solve(a, b)[, 1]  # Coefficients from the covariance matrix
beta.hat
#
# 2a. Find the intercept from the means and coefficients.
#
y.bar <- mean(y)
y.bar
x.bar <- colMeans(x)
x.bar
intercept <- y.bar - x.bar %*% beta.hat  
intercept

(rbind(`From covariances` = c(`(Intercept)`=intercept, beta.hat),
       `From data via OLS` = coef(lm(y ~ x))))

# Los casos que nos preocupan son los 0 o NA ¿Por qué nos da esos resultados?
# del algoritmo LV IDA global revisar porque salen 0s
# 'Estimating bounds on causal effects in high-dimensional and possibly confounded systems'
# Ecuaciones estructurales
# Revisar los datasets de la tabla del artículo "A Survey of Learning Causality with Data- Problems and Methods"
## Como han utilizado estos datasets

####### DATASETS

# PROMO

