def crop_images(aux_crop):
	s = aux_crop.shape
	s = s[1]
	if (s == 62):
		aux = crop_image_62(aux_crop)
	elif (s==55):
		aux = crop_image_55(aux_crop)
	elif(s == 48):
		aux = crop_image_48(aux_crop)
	elif (s == 41):
		aux = crop_image_41(aux_crop)
	else:
		print("Error en la identificación del tamaño de imagen")
	return aux

def crop_image_62(aux_crop):
	n = img_list[11].shape
	n = n[1]-1
	aux = []
	aux.append(aux_crop[:,n-7:n])
	aux.append(aux_crop[:,n-2*7:n-7])
	aux.append(aux_crop[:,n-3*7:n-2*7])
	aux.append(aux_crop[:,n-4*7:n-3*7])
	aux.append(aux_crop[:,64-5*7:62-4*7])
	aux.append(aux_crop[:,n+4-6*7:n+4-5*7])
	aux.append(aux_crop[:,n+4-7*7:n+4-6*7])
	aux.append(aux_crop[:,n+4-8*7:n+4-7*7])
	aux.append(aux_crop[:,:n+4-8*7])
	return aux



def crop_image_55(aux_crop):
	n = img_list[11].shape
	n = n[1]-1
	aux = []
	aux.append(aux_crop[:,n-7:n])
	aux.append(aux_crop[:,n-2*7:n-7])
	aux.append(aux_crop[:,n-3*7:n-2*7])
	aux.append(aux_crop[:,n-4*7:n-3*7])
	aux.append(aux_crop[:,64-5*7:62-4*7])
	aux.append(aux_crop[:,n+4-6*7:n+4-5*7])
	aux.append(aux_crop[:,n+4-7*7:n+4-6*7])
	aux.append(aux_crop[:,n+4-8*7:n+4-7*7])
	return aux


def crop_image_48(aux_crop):
	n = img_list[11].shape
	n = n[1]-1
	aux = []
	aux.append(aux_crop[:,n-7:n])
	aux.append(aux_crop[:,n-2*7:n-7])
	aux.append(aux_crop[:,n-3*7:n-2*7])
	aux.append(aux_crop[:,n-4*7:n-3*7])
	aux.append(aux_crop[:,64-5*7:62-4*7])
	aux.append(aux_crop[:,n+4-6*7:n+4-5*7])
	aux.append(aux_crop[:,n+4-7*7:n+4-6*7])
	return aux


def crop_image_41(aux_crop):
	n = img_list[11].shape
	n = n[1]-1
	aux = []
	aux.append(aux_crop[:,n-7:n])
	aux.append(aux_crop[:,n-2*7:n-7])
	aux.append(aux_crop[:,n-3*7:n-2*7])
	aux.append(aux_crop[:,n-4*7:n-3*7])
	aux.append(aux_crop[:,64-5*7:62-4*7])
	aux.append(aux_crop[:,n+4-6*7:n+4-5*7])
	return aux

